// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "freecad.hh"

// code specific to Rat
namespace rat{namespace cmn{

	// constructor
	FreeCAD::FreeCAD(const boost::filesystem::path &fname, const std::string &model_name){
		// add extension
		boost::filesystem::path fname_ext = fname;
		fname_ext.replace_extension("FCMacro");

		// open file for writing
		fid_.open(fname_ext.string(), std::ios::binary);
		
		// ensure file is open
		if(!fid_.is_open())rat_throw_line("could not open file for writing");

		// set precision
		fid_ << std::fixed;
		fid_ << std::setprecision(16);

		// header write
		write_header(model_name);

		// gui update
		// enable_gui_update_ = true;

		// enable union
		// create_union_ = true;
		enable_refine_ = true;
	}

	// destructor
	FreeCAD::~FreeCAD(){
		// write footer
		write_footer();

		// close file
		if(fid_.is_open())fid_.close();
	}

	// factory
	ShFreeCADPr FreeCAD::create(
		const boost::filesystem::path &fname, 
		const std::string &model_name){
		//return ShFreeCADPr(new FreeCAD);
		return std::make_shared<FreeCAD>(fname,model_name);
	}

	// setters
	void FreeCAD::set_num_sub(const arma::uword num_sub){
		num_sub_ = num_sub;
	}

	void FreeCAD::set_no_graphics(const bool no_graphics){
		no_graphics_ = no_graphics;
	}

	void FreeCAD::set_num_max(const arma::uword num_max){
		num_max_ = num_max;
	}

	void FreeCAD::set_num_refine_max(const arma::uword num_refine_max){
		num_refine_max_ = num_refine_max;
	}


	void FreeCAD::set_keep_construction(const bool keep_construction){
		keep_construction_ = keep_construction;
	}
	
	void FreeCAD::set_create_union(const bool create_union){
		create_union_ = create_union;
	}
	
	void FreeCAD::set_enable_gui_update(const bool enable_gui_update){
		enable_gui_update_ = enable_gui_update;
	}

	void FreeCAD::set_override_ruled_surface(const bool override_ruled_surface){
		override_ruled_surface_ = override_ruled_surface;
	}

	void FreeCAD::set_enable_refine(const bool enable_refine){
		enable_refine_ = enable_refine;
	}

	void FreeCAD::set_scale(const fltp scale){
		scale_ = scale;
	}

	// write point list
	void FreeCAD::write_header(const std::string &model_name){
		// write header
		fid_<<"import FreeCAD,Draft,Part,Import\n";
		fid_<<"doc = App.newDocument(\""<<model_name<<"\")\n";
		fid_<<"doc.UndoMode = 0\n";
		fid_<<"export=[]\n";
		fid_<<"parts=[]\n";
		fid_<<"lines=[]\n";
		fid_<<"group=[]\n";
	}

	// append group
	void FreeCAD::append_group(const std::string &group_name){
		// add the group to the list
		fid_<<"if len(group) == 0:\n";
		fid_<<"\tgr = doc.addObject(\"App::DocumentObjectGroup\",\""<<group_name<<"\")\n";
		fid_<<"\tgroup.append(gr)\n";
		fid_<<"else:\n";
		fid_<<"\tgr = doc.addObject(\"App::DocumentObjectGroup\",\""<<group_name<<"\")\n";
		fid_<<"\tgroup[-1].addObject(gr)\n";
		fid_<<"\tgroup.append(gr)\n";
	}

	// pop group
	void FreeCAD::pop_group(){
		fid_<<"group.pop()\n";
	}

	// write edges
	void FreeCAD::write_edges( 
		const arma::field<arma::Mat<fltp> > &x,
		const arma::field<arma::Mat<fltp> > &y, 
		const arma::field<arma::Mat<fltp> > &z,
		const arma::Row<arma::uword> &section_idx,
		const arma::Row<arma::uword> &turn_idx,
		const std::string &coil_name, 
		const arma::Col<float>::fixed<3>& color){

		// check for empty
		if(x.is_empty() || y.is_empty() || z.is_empty())return;

		// create group with same name as coil
		// fid_<<"gr = doc.addObject(\"App::DocumentObjectGroup\",\""<<coil_name<<"\")\n";
		// fid_<<"gr = doc.addObject('App::Part','"<<coil_name<<"')\n";

		// avoid same names
		const arma::uword cnt = ++coil_name_count_[coil_name];
		std::string coil_name_ext = coil_name;
		if(cnt>1)coil_name_ext += "_" + std::to_string(cnt);

		// walk over sections
		for(arma::uword i=0;i<x.n_elem;i++){
			// number of sub sections
			arma::uword num_sub = std::max(num_sub_,1llu);

			// number of subdivisions 
			arma::uword num_write = 0;
			for(;;){
				num_write = x(i).n_cols/num_sub + 1;
				if(num_write>num_max_)num_sub*=2; else break;
			}

			// number of sub sections
			arma::uword num_sub_int = x(i).n_cols/num_write + 1;

			// subdivide section until criterion reached
			if(enable_refine_ && x(i).n_rows==4 && x(i).n_cols>2 && num_refine_max_>1){
				// re-create longitudinal vector using finite difference
				const arma::Mat<fltp> Le = arma::join_vert(
					arma::diff(x(i).row(0),1,1),
					arma::diff(y(i).row(0),1,1),
					arma::diff(z(i).row(0),1,1));
				arma::Mat<fltp> L(3,x(i).n_cols,arma::fill::zeros);
				L.head_cols(x(i).n_cols-1) += Le; 
				L.tail_cols(x(i).n_cols-1) += Le;
				L.cols(1,x(i).n_cols-2)/=2;

				// refine
				for(arma::uword k=0;k<num_refine_max_;k++){
					// calculate subdivisions
					num_write = x(i).n_cols/num_sub + 1;

					// calculate number of subsections
					num_sub_int = x(i).n_cols/num_write + 1;

					// write to section
					arma::Row<fltp> angle(num_sub_int, arma::fill::zeros);
					for(arma::uword j=0;j<num_sub_int;j++){
						// walk over subsections
						const arma::uword idx1 = std::min(x(i).n_cols-1,j*num_write);
						const arma::uword idx2 = std::min(x(i).n_cols-1,(j+1)*num_write);

						// repeat first vector
						const arma::Mat<fltp> L0 = arma::repmat(L.col(idx1),1,idx2-idx1+1);

						// calculate angles between vectors and first vector
						angle(j) = std::max(angle(j), arma::max( Extra::vec_angle(L0,L.cols(idx1,idx2)) ));
					}

					// check if angle smaller than 90 degrees
					if(arma::max(angle)>RAT_CONST(1.000001)*arma::Datum<fltp>::pi/2)num_sub*=2; else break;
				}
			}

			// write to section
			for(arma::uword j=0;j<num_sub_int;j++){
				// walk over subsections
				const arma::uword idx1 = j*num_write;
				const arma::uword idx2 = std::min(x(i).n_cols-1,(j+1)*num_write);
				if(idx2<=idx1)continue;
				assert(idx2<x(i).n_cols);

				// get edge data matrices for this sub-section
				arma::Mat<fltp> xx = x(i).cols(idx1,idx2);
				arma::Mat<fltp> yy = y(i).cols(idx1,idx2);
				arma::Mat<fltp> zz = z(i).cols(idx1,idx2);

				// // check which edge is longer
				// const fltp dx1 = xx(1,0) - xx(0,0), dx2 = xx(2,0) - xx(1,0);
				// const fltp dy1 = yy(1,0) - yy(0,0), dy2 = yy(2,0) - yy(1,0);
				// const fltp dz1 = zz(1,0) - zz(0,0), dz2 = zz(2,0) - zz(1,0);
				// const fltp ell1 = std::sqrt(dx1*dx1 + dy1*dy1 + dz1*dz1);
				// const fltp ell2 = std::sqrt(dx2*dx2 + dy2*dy2 + dz2*dz2);

				// // perform shifting if needed
				// if(ell2<ell1){
				// 	xx = arma::shift(xx,1,0);
				// 	yy = arma::shift(yy,1,0);
				// 	zz = arma::shift(zz,1,0);
				// }

				// write section
				// if(xx.n_rows==1 || xx.n_rows==2 || xx.n_rows==4)
				// write_section(xx,yy,zz,section_idx(i),turn_idx(i),j,coil_name_ext);
				// write_section2(xx,yy,zz,section_idx(i),turn_idx(i),j,coil_name_ext);
				const fltp angular_tolerance = arma::Datum<fltp>::pi/180/10;
				
				// single edge (i.e. a line)
				if(xx.n_rows==1){
					write_section(xx,yy,zz,section_idx(i),turn_idx(i),j,coil_name_ext,color);
				}

				// surface mesh with 2 edges
				else if(xx.n_rows==2){
					if(is_ruled_surface(xx,yy,zz,0,1,angular_tolerance) || override_ruled_surface_){
						write_section(xx,yy,zz,section_idx(i),turn_idx(i),j,coil_name_ext,color);
					}else{
						write_section3(xx,yy,zz,section_idx(i),turn_idx(i),j,coil_name_ext,color);
					}
				}

				// volume mesh with four edges
				else if(xx.n_rows==4){
					if((is_ruled_surface(xx,yy,zz,0,1,angular_tolerance) && 
						is_ruled_surface(xx,yy,zz,2,3,angular_tolerance)) || override_ruled_surface_){
						write_section(xx,yy,zz,section_idx(i),turn_idx(i),j,coil_name_ext,color);
					}else{
						write_section3(xx,yy,zz,section_idx(i),turn_idx(i),j,coil_name_ext,color);
					}
				}

				// 3 edges or more than 4 edges must try to use loft method
				else{
					write_section2(xx,yy,zz,section_idx(i),turn_idx(i),j,coil_name_ext,color);
				}
			}
		}

		// unify
		if(create_union_)
			create_fusion(coil_name);
		else
			group_parts(coil_name);
	}

	// name making function
	std::string FreeCAD::create_section_name(
		const arma::uword turn_idx,
		const arma::uword section_idx, 
		const arma::uword sub_idx){

		// make name
		std::stringstream str;
		str<<"_turn"; str.width(3); str.fill('0'); str<<turn_idx;
		str<<"_sect"; str.width(3); str.fill('0'); str<<section_idx;
		str<<"_sub"; str.width(3); str.fill('0'); str<<sub_idx;

		// return string
		return str.str();
	}

	// export the geometry
	// by connecting edges with ruled surface
	// then use loft to connect the two ruled surfaces
	// only works with 1,2 or 4 edges. Also this method,
	// due to the use of a ruled surface, assumes that 
	// all generator lines are perpendicular to the edges
	// of the cable. It is therefore not a good choice for
	// frenet-serret frame.
	void FreeCAD::write_section(
		const arma::Mat<fltp> &x,
		const arma::Mat<fltp> &y, 
		const arma::Mat<fltp> &z,
		const arma::uword section_idx, 
		const arma::uword turn_idx,
		const arma::uword sub_idx,
		const std::string &coil_name,
		const arma::Col<float>::fixed<3>& color){

		// check for empty
		assert(!x.is_empty());
		assert(!y.is_empty());
		assert(!z.is_empty());
		assert(x.n_rows==1 || x.n_rows==2 || x.n_rows==4);
		assert(y.n_rows==x.n_rows);
		assert(z.n_rows==x.n_rows);

		// get number of edges
		const arma::uword num_edges = x.n_rows;
		const arma::uword num_coords = x.n_cols;

		// header
		fid_<<"# section from ruled surfaces\n";

		// check if closed
		const bool closed = is_closed(x,y,z,1e-9);

		// walk over edges
		for(arma::uword k=0;k<num_edges;k++){

			// walk over coordinates
			for(arma::uword j=0;j<num_coords-int(closed);j++){
				fid_<<"p"<<j<<" = FreeCAD.Vector(";
				fid_<<scale_*x(k,j)<<","<<scale_*y(k,j)<<","<<scale_*z(k,j);
				fid_<<")\n";
			}

			// create spline
			fid_<<"spline"<<k<<" = Draft.make_bspline([";
			write_pointlist(x.n_cols-int(closed));
			fid_<<"],closed="<<(closed ? "True" : "False")<<",face=False)\n";

			// set view colors etc
			if(!no_graphics_){
				fid_<<"spline"<<k<<".ViewObject.LineColor = (1.0,1.0,1.0,0.0)\n";
				fid_<<"spline"<<k<<".ViewObject.LineWidth = 0.5\n";
			}
		}

		// single edge
		if(num_edges==1){
			// add object to group
			fid_<<"lines.append(spline0)\n";
		}

		// is at least surface
		else if(num_edges==2){
			// splines are no longer visible
			if(!no_graphics_){
				fid_<<"spline0.ViewObject.Visibility = False\n";
				fid_<<"spline1.ViewObject.Visibility = False\n";
			}

			// first ruled surface
			fid_<<"rule0 = doc.addObject('Part::RuledSurface','rule0')\n";
			fid_<<"rule0.Curve1 = (spline0,[''])\n";
			fid_<<"rule0.Curve2 = (spline1,[''])\n";

			// set view colors etc
			if(!no_graphics_){
				fid_<<"rule0.ViewObject.LineColor = (1.0,1.0,1.0,0.0)\n";
				fid_<<"rule0.ViewObject.LineWidth = 0.5\n";
				fid_<<"rule0.ViewObject.ShapeColor = ("<<color(0)<<","<<color(1)<<","<<color(2)<<",0.0)\n";
			}

			// add object to group
			fid_<<"parts.append(rule0)\n";
		}

		// is volume mesh
		else if(num_edges==4){
			// splines are no longer visible
			if(!no_graphics_){
				fid_<<"spline0.ViewObject.Visibility = False\n";
				fid_<<"spline1.ViewObject.Visibility = False\n";
				fid_<<"spline2.ViewObject.Visibility = False\n";
				fid_<<"spline3.ViewObject.Visibility = False\n";
			}

			// first ruled surface
			fid_<<"rule0 = doc.addObject('Part::RuledSurface','rule0')\n";
			fid_<<"rule0.Curve1 = (spline0,[''])\n";
			fid_<<"rule0.Curve2 = (spline1,[''])\n";

			// second ruled surface
			fid_<<"rule1 = doc.addObject('Part::RuledSurface','rule1')\n";
			fid_<<"rule1.Curve1 = (spline2,[''])\n";
			fid_<<"rule1.Curve2 = (spline3,[''])\n";

			// set view colors etc
			if(!no_graphics_){
				fid_<<"rule0.ViewObject.LineColor = (1.0,1.0,1.0,0.0)\n";
				fid_<<"rule0.ViewObject.LineWidth = 0.5\n";
				fid_<<"rule0.ViewObject.ShapeColor = ("<<color(0)<<","<<color(1)<<","<<color(2)<<",0.0)\n";
				fid_<<"rule0.ViewObject.Visibility = False\n";
				fid_<<"rule1.ViewObject.LineColor = (1.0,1.0,1.0,0.0)\n";
				fid_<<"rule1.ViewObject.LineWidth = 0.5\n";
				fid_<<"rule1.ViewObject.ShapeColor = ("<<color(0)<<","<<color(1)<<","<<color(2)<<",0.0)\n";
				fid_<<"rule1.ViewObject.Visibility = False\n";
			}

			// % loft
			fid_<<"loft = doc.addObject('Part::Loft','"<<coil_name<<create_section_name(turn_idx, section_idx, sub_idx)<<"')\n";
			fid_<<"loft.Sections = [rule0,rule1]\n";
			fid_<<"loft.Solid = True\n";
			fid_<<"loft.Ruled = False\n";

			// disable face visibility
			if(!no_graphics_){
				fid_<<"rule0.ViewObject.Visibility = False\n";
				fid_<<"rule1.ViewObject.Visibility = False\n";
			}

			// set view colors etc
			if(!no_graphics_){
				fid_<<"loft.ViewObject.LineColor = (1.0,1.0,1.0,0.0)\n";
				fid_<<"loft.ViewObject.LineWidth = 0.5\n";
				fid_<<"loft.ViewObject.ShapeColor = ("<<color(0)<<","<<color(1)<<","<<color(2)<<",0.0)\n";
			}

			// add to parts
			fid_<<"parts.append(loft)\n";

			// // dump into group
			// if(!create_union_){
			// 	fid_<<"if len(group) != 0:\n";
			// 	fid_<<"\tgroup[-1].addObject(loft)\n";
			// }
		}

		// update gui
		if(enable_gui_update_){
			fid_<<"doc.recompute()\n";
			fid_<<"FreeCADGui.ActiveDocument.ActiveView.fitAll()\n";
			fid_<<"FreeCADFreeCADGui.updateGui()\n";
		}
	}


	// export the geometry by drawing first 
	// the cross sections then connecting the
	// cross sections with a loft 
	// this method can deal with any number of 
	// edges. It does try to determine whether
	// an edge is a sharp corner or not by checking
	// its angle
	void FreeCAD::write_section2(
		const arma::Mat<fltp> &x,
		const arma::Mat<fltp> &y, 
		const arma::Mat<fltp> &z,
		const arma::uword section_idx, 
		const arma::uword turn_idx,
		const arma::uword sub_idx,
		const std::string &coil_name,
		const arma::Col<float>::fixed<3>& color){

		// check for empty
		assert(!x.is_empty());
		assert(!y.is_empty());
		assert(!z.is_empty());
		assert(y.n_rows==x.n_rows);
		assert(z.n_rows==x.n_rows);

		// header
		fid_<<"# section by lofting cross sections\n";

		// get number of edges
		const arma::uword num_edges = x.n_rows;
		const arma::uword num_coords = x.n_cols;

		// walk over profiles
		for(arma::uword j=0;j<num_coords;j++){
			// calculate angles between consecutive elements
			arma::Mat<fltp> R = arma::join_horiz(x.col(j),y.col(j),z.col(j)).t();
			R = arma::join_horiz(R,R.col(0));
			arma::Mat<fltp> dR = arma::diff(R,1,1);
			dR = arma::join_horiz(dR.tail_cols(1),dR); // connect last to first
			dR.each_row()/=Extra::vec_norm(dR); // normalize
			arma::Row<fltp> alpha = Extra::vec_angle(dR.head_cols(num_edges),dR.tail_cols(num_edges));

			// find indices of edges that exeed the angle 
			// treshold and are thus considered corners
			const fltp angle_treshold = RAT_CONST(40.0)*arma::Datum<fltp>::tau/RAT_CONST(360.0);
			arma::Row<arma::uword> mark_corner = arma::abs(alpha)>angle_treshold;
			const arma::Row<arma::uword> drop_points = arma::abs(alpha)<arma::Datum<fltp>::pi/180/100;

			// find edges that are perfectly flat
			mark_corner = (mark_corner + arma::shift(drop_points,1,1) + drop_points + arma::shift(drop_points,-1,1))>0 && drop_points==0;

			// find corners
			const arma::Col<arma::uword> idx_corner = arma::find(mark_corner);

			// vertices
			for(arma::uword k=0;k<num_edges;k++){
				// create points
				fid_<<"p"<<k<<" = FreeCAD.Vector(";
				fid_<<scale_*x(k,j)<<","<<scale_*y(k,j)<<","<<scale_*z(k,j);
				fid_<<")\n";
			}

			// no corners it is a closed spline
			arma::uword num_splines = 0;
			if(idx_corner.is_empty()){
				// create single spline
				fid_<<"spline0"<<"=Draft.make_bspline([";
				for(arma::uword k=0;k<num_edges;k++){
					fid_<<"p"<<k;
					if(k!=num_edges-1)fid_<<",";
				}
				// fid_<<",p0";
				fid_<<"],closed=True,placement=None,face=False,support=None)\n";
				num_splines = 1;
			}

			else{
				// walk over sections
				num_splines = idx_corner.n_elem;
				for(arma::uword k=0;k<num_splines;k++){
					// get looping indices
					const arma::uword idx1 = idx_corner(k);
					const arma::uword idx2 = idx_corner((k+1)%idx_corner.n_elem);

					// more than two points use smooth spline
					if(idx2 - idx1 + 1>2){
						fid_<<"spline"<<k<<"=Draft.make_bspline([";
						for(arma::uword l=idx1;;l++){
							if(l>=num_edges)l=0;
							fid_<<"p"<<l;
							if(l==idx2)break;
							fid_<<",";
						}
						fid_<<"],closed=False,placement=None,face=False,support=None)\n";
					}

					// two points create straight line
					else{
						// connect points with line
						fid_<<"spline"<<k<<"=Draft.make_line(p"<<idx1<<",p"<<idx2<<")\n";
					}
				}
			}

			// recompute
			fid_<<"doc.recompute()\n";

			// hide splines
			if(!no_graphics_)
				for(arma::uword i=0;i<num_splines;i++)
					fid_<<"spline"<<i<<".ViewObject.Visibility = False\n";

			// create wire
			fid_<<"wire=Part.Wire(Part.__sortEdges__([";
			for(arma::uword i=0;i<num_splines;i++){
				fid_<<"spline"<<i<<".Shape.Edge1";
				if(i!=num_splines-1)fid_<<",";
			}
			fid_<<"]))\n";

			// create face between splines
			fid_<<"face"<<j<<"=doc.addObject('Part::Feature', 'Face')\n";
			fid_<<"face"<<j<<".Shape = Part.Face(wire)\n";

			// delete splines
			for(arma::uword k=0;k<num_splines;k++)
				fid_<<"doc.removeObject(spline"<<k<<".Name)\n";

			// allow update gui
			if(enable_gui_update_){
				fid_<<"doc.recompute()\n";
				fid_<<"FreeCADGui.ActiveDocument.ActiveView.fitAll()\n";
				fid_<<"FreeCADGui.updateGui()\n";
			}
		}

		// create a loft from the faces
		fid_<<"loft = doc.addObject('Part::Loft','"<<coil_name<<create_section_name(turn_idx, section_idx, sub_idx)<<"')\n";
		fid_<<"loft.Sections = [";
		for(arma::uword j=0;j<num_coords;j++){
			fid_<<"face"<<j;
			if(j!=num_coords-1)fid_<<",";
		}
		fid_<<"]\n";
		fid_<<"loft.Solid = True\n";
		fid_<<"loft.Ruled = False\n";

		// disable face visibility
		if(!no_graphics_)
			for(arma::uword j=0;j<num_coords;j++)
				fid_<<"face"<<j<<".ViewObject.Visibility = False\n";

		// set view colors etc
		if(!no_graphics_){
			fid_<<"loft.ViewObject.LineColor = (1.0,1.0,1.0,0.0)\n";
			fid_<<"loft.ViewObject.LineWidth = 0.5\n";
			fid_<<"loft.ViewObject.ShapeColor = ("<<color(0)<<","<<color(1)<<","<<color(2)<<",0.0)\n";
		}

		// add object to group
		fid_<<"parts.append(loft)\n";


		// // dump into group
		// if(!create_union_){
		// 	fid_<<"if len(group) != 0:\n";
		// 	fid_<<"\tgroup[-1].addObject(loft)\n";
		// }

		// update gui
		if(enable_gui_update_){
			fid_<<"doc.recompute()\n";
			fid_<<"FreeCADGui.ActiveDocument.ActiveView.fitAll()\n";
			fid_<<"FreeCADGui.updateGui()\n";
		}
	}

	
	// export the geometry by drawing the generator lines
	// then connecting them with a surface. IN contrast to 
	// the first method the lines can have an angle with
	// respect to the edges. Therefore it works also for
	// Frenet-Serret frame. But somehow this method
	// is a bit more unstable to use.
	void FreeCAD::write_section3(
		const arma::Mat<fltp> &x,
		const arma::Mat<fltp> &y, 
		const arma::Mat<fltp> &z,
		const arma::uword section_idx, 
		const arma::uword turn_idx,
		const arma::uword sub_idx,
		const std::string &coil_name,
		const arma::Col<float>::fixed<3>& color){

		// check for empty
		assert(!x.is_empty());
		assert(!y.is_empty());
		assert(!z.is_empty());
		assert(x.n_rows==2 || x.n_rows==4);
		assert(y.n_rows==x.n_rows);
		assert(z.n_rows==x.n_rows);

		// header
		fid_<<"# section by generators\n";

		// get number of edges
		const arma::uword num_edges = x.n_rows;
		const arma::uword num_coords = x.n_cols;

		// walk over surfaces
		for(arma::uword j=0;j<num_edges/2;j++){
			// indices
			const arma::uword idx1 = j==0 ? 1 : 0;
			const arma::uword idx2 = j==0 ? 2 : 3;

			// first side
			for(arma::uword k=0;k<num_coords;k++){
				// first point
				fid_<<"p1"<<"=FreeCAD.Vector(";
				fid_<<scale_*x(idx1,k)<<","<<scale_*y(idx1,k)<<","<<scale_*z(idx1,k);
				fid_<<")\n";

				// second point
				fid_<<"p2"<<"=FreeCAD.Vector(";
				fid_<<scale_*x(idx2,k)<<","<<scale_*y(idx2,k)<<","<<scale_*z(idx2,k);
				fid_<<")\n";

				// connect points with line
				fid_<<"line"<<k<<"=Draft.make_line(p1,p2)\n";

				// set view colors etc
				if(!no_graphics_){
					fid_<<"line"<<k<<".ViewObject.LineColor = (1.0,1.0,1.0,0.0)\n";
					fid_<<"line"<<k<<".ViewObject.LineWidth = 0.5\n";
				}

				// allow update gui
				if(enable_gui_update_){
					fid_<<"doc.recompute()\n";
					fid_<<"FreeCADGui.ActiveDocument.ActiveView.fitAll()\n";
					fid_<<"FreeCADGui.updateGui()\n";
				}
			}

			// create a loft from the faces
			fid_<<"surf"<<j<<" = doc.addObject('Part::Loft','Surface"<<j<<"')\n";
			fid_<<"surf"<<j<<".Sections = [";
			for(arma::uword k=0;k<num_coords;k++){
				fid_<<"line"<<k;
				if(j!=num_coords-1)fid_<<",";
			}
			fid_<<"]\n";
			fid_<<"surf"<<j<<".Solid = False\n";
			fid_<<"surf"<<j<<".Ruled = False\n";

			// disable visibility for lines
			if(!no_graphics_)
				for(arma::uword k=0;k<num_coords;k++)
					fid_<<"line"<<k<<".ViewObject.Visibility = False\n";

			// allow update gui
			if(enable_gui_update_){
				fid_<<"doc.recompute()\n";
				fid_<<"FreeCADGui.ActiveDocument.ActiveView.fitAll()\n";
				fid_<<"FreeCADGui.updateGui()\n";
			}
		}

		// create loft
		if(num_edges==4){
			// create a loft from the faces
			fid_<<"loft = doc.addObject('Part::Loft','"<<coil_name<<create_section_name(turn_idx, section_idx, sub_idx)<<"')\n";
			fid_<<"loft.Sections = [surf0,surf1]\n";
			fid_<<"loft.Solid = True\n";
			fid_<<"loft.Ruled = True\n";
		}

		if(!no_graphics_)
			for(arma::uword j=0;j<num_edges/2;j++)
				fid_<<"surf"<<j<<".ViewObject.Visibility = False\n";

		// set view colors etc
		if(!no_graphics_){
			fid_<<"loft.ViewObject.LineColor = (1.0,1.0,1.0,0.0)\n";
			fid_<<"loft.ViewObject.LineWidth = 0.5\n";
			fid_<<"loft.ViewObject.ShapeColor = ("<<color(0)<<","<<color(1)<<","<<color(2)<<",0.0)\n";
		}

		// add to parts
		fid_<<"parts.append(loft)\n";

		// dump into group 
		// if(!create_union_){
		// 	fid_<<"if len(group) != 0:\n";
		// 	fid_<<"\tgroup[-1].addObject(loft)\n";
		// }

		// update gui
		if(enable_gui_update_){
			fid_<<"doc.recompute()\n";
			fid_<<"FreeCADGui.ActiveDocument.ActiveView.fitAll()\n";
			fid_<<"FreeCADGui.updateGui()\n";
		}
	}

	// create union
	void FreeCAD::create_fusion(const std::string& coil_name){
		// join lines
		fid_<<"if len(lines) != 0:\n";
		fid_<<"\tDraft.join_wires(lines)\n";
		fid_<<"\tDraft.join_wires(lines)\n";
		fid_<<"\tlines = []\n";

		// check if more than one shape
		fid_<<"if len(parts)>1:\n";
		
		// create fuse
		fid_<<"\tfuse = doc.addObject('Part::MultiFuse','"<<"fuse_"<<coil_name<<"')\n";
		fid_<<"\tfuse.Shapes = parts\n";

		// set view colors etc
		if(!no_graphics_){
			fid_<<"\tfuse.ViewObject.LineColor = parts[0].ViewObject.LineColor\n";
			fid_<<"\tfuse.ViewObject.LineWidth = parts[0].ViewObject.LineWidth \n";
			fid_<<"\tfuse.ViewObject.ShapeColor = parts[0].ViewObject.ShapeColor\n";
		}

		// add to export list
		fid_<<"\texport.append(fuse)\n";

		// dump into group
		fid_<<"\tif len(group) != 0:\n";
		fid_<<"\t\tgroup[-1].addObject(fuse)\n";

		// single object case
		fid_<<"elif len(parts)==1:\n";
		fid_<<"\texport.append(parts[0])\n";

		// add part itself to group
		fid_<<"\tif len(group) != 0:\n";
		fid_<<"\t\tgroup[-1].addObject(parts[0])\n";

		// clear part list
		fid_<<"parts = []\n";
	}

	// create union
	void FreeCAD::group_parts(const std::string& coil_name){
		// check if more than one shape
		fid_<<"if len(parts)>1:\n";
		
		// dunk parts into a group
		fid_<<"\tgr = doc.addObject(\"App::DocumentObjectGroup\",\""<<coil_name<<"\")\n";
		fid_<<"\tif len(group) != 0:\n";
		fid_<<"\t\tgroup[-1].addObject(gr)\n";
		fid_<<"\tfor x in parts:\n";
		fid_<<"\t\tgr.addObject(x)\n";
		fid_<<"\t\texport.append(x)\n";

		// clear part list
		fid_<<"parts = []\n";
	}


	// save document
	void FreeCAD::save_doc(const boost::filesystem::path &fname){
		fid_<<"doc.recompute()\n";
		fid_<<"doc.saveAs('"<<fname.generic_string()<<"')\n"; // generic string for python should be fine
	}

	// request save
	void FreeCAD::save_step_file(const boost::filesystem::path &fname){
		// fid_<<"doc.recompute()\n";
		// fid_<<"Part.export(export,'"<<fname.string()<<"')\n";

		fid_<<"doc.recompute()\n";
		fid_<<"Import.export(export,'"<<fname.generic_string()<<"')\n"; // generic string for python should be fine
	}


	// write point list
	void FreeCAD::write_pointlist(arma::uword num_points){
		// safety
		if(num_points==0)return;

		// walk over points (except last)
		for(arma::uword j=0;j<num_points-1;j++){
			fid_<<"p"<<j<<",";
		}

		// last point
		fid_<<"p"<<num_points-1;
	} 

	// write footer to file
	void FreeCAD::write_footer(){
		fid_<<"doc.UndoMode = 1\n";
		fid_<<"doc.recompute()\n";
		fid_<<"del export\n";
		if(!no_graphics_){
			// set fit
			fid_<<"FreeCADGui.ActiveDocument.ActiveView.fitAll()\n";

			// force update
			fid_<<"FreeCADGui.updateGui()\n";
		}
		else{
			fid_<<"exit()\n";
		}
	}

	// method for checking if two edges are ruled surface
	bool FreeCAD::is_ruled_surface(
		const arma::Mat<fltp>& xx, 
		const arma::Mat<fltp>& yy, 
		const arma::Mat<fltp>& zz, 
		const arma::uword idx1, 
		const arma::uword idx2, 
		const rat::fltp tol){

		if(xx.n_cols<=2)return true;

		// get edge lines
		const arma::Mat<fltp> R1 = arma::join_vert(xx.row(idx1), yy.row(idx1), zz.row(idx1));
		const arma::Mat<fltp> R2 = arma::join_vert(xx.row(idx2), yy.row(idx2), zz.row(idx2));
		
		// reconstruct vectors and normalize
		const arma::Mat<fltp> L = Extra::normalize(arma::diff(R1,1,1)); 
		const arma::Mat<fltp> D = Extra::normalize(R2 - R1);

		// take average
		const arma::Mat<fltp> Lav = (L.head_cols(L.n_cols-1) + L.tail_cols(L.n_cols-1))/2;

		// calculate angle of generators
		const arma::Row<fltp> angle = Extra::vec_angle(Lav,D.cols(1,D.n_cols-2));

		// check angles
		return arma::all(arma::abs(angle - arma::Datum<fltp>::pi/2)<tol);
	}

	// is closed
	bool FreeCAD::is_closed(
		const arma::Mat<fltp>& x, 
		const arma::Mat<fltp>& y, 
		const arma::Mat<fltp>& z, 
		const rat::fltp tol){

		// check if start point equals the end point
		return arma::all(arma::abs(x.col(0) - x.col(x.n_cols-1))<tol) && 
			arma::all(arma::abs(y.col(0) - y.col(y.n_cols-1))<tol) && 
			arma::all(arma::abs(z.col(0) - z.col(z.n_cols-1))<tol);
	}

}}