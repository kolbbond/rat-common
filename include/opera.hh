// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef CMN_OPERA_HH
#define CMN_OPERA_HH

// general headers
#include <armadillo> 
#include <memory>
#include <iostream>
#include <iomanip>
#include <cassert>
#include <boost/filesystem.hpp>

#include "typedefs.hh"
#include "error.hh"

// code specific to Rat
namespace rat{namespace cmn{
	// shared pointer definition
	typedef std::shared_ptr<class Opera> ShOperaPr;

	// template for coil
	class Opera{
		// properties
		protected:
			// file out stream
			std::ofstream fid_; 

			// set scaling
			fltp scale_ = 1000;

			// drive counter
			arma::uword drive_idx_;

		// methods
		public:
			// constructor
			explicit Opera(const boost::filesystem::path &fname);
			
			// destructor
			~Opera();

			// factory
			static ShOperaPr create(const boost::filesystem::path &fname);
					
			// write opera conductor file
			void write_edges(
				const arma::field<arma::Mat<fltp> > &x, 
				const arma::field<arma::Mat<fltp> > &y, 
				const arma::field<arma::Mat<fltp> > &z, 
				const fltp J);
	};

}}

#endif
