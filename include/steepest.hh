// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// adapted from: https://ch.mathworks.com/matlabcentral/fileexchange/43097-newton-raphson-solver

// include guard
#ifndef CMN_STEEPEST_HH
#define CMN_STEEPEST_HH

// general headers
#include <armadillo> 
#include <memory>
#include <iomanip>
#include <functional>
#include <cassert>

// specific headers
#include "typedefs.hh"
#include "error.hh"
#include "parfor.hh"
#include "log.hh"

// code specific to Rat
namespace rat{namespace cmn{

	// shared pointer definition
	typedef std::shared_ptr<class Steepest> ShSteepestPr;

	// gradient function
	typedef std::function<arma::Col<fltp>(const arma::Col<fltp>&) > StGradFun;

	// system function
	typedef std::function<fltp(const arma::Col<fltp>&) > StSysFun;

	// newton raphson non-linear solver
	class Steepest{
		// properties
		private:
			// settings
			fltp delta_ = RAT_CONST(1e-6);
			fltp gamma_ = RAT_CONST(0.5);
			fltp tolx_ = RAT_CONST(1e-4);
			arma::uword num_iter_max_ = 1000;
			bool use_parallel_ = false;
			bool use_linesearch_ = true;
			bool use_central_diff_ = false;

			// functions describing problem
			StSysFun systemfun_;
			StGradFun gradientfun_;
			
			// solver 
			arma::Col<fltp> x0_;
			arma::Col<fltp> x_;
			fltp fval_;

		// methods
		public:
			// constructor
			Steepest();

			// factory
			static ShSteepestPr create();

			// set system function to optimise
			void set_systemfun(StSysFun systemfun);

			// set/or assemble gradient function
			void set_finite_difference();

			// set/or assemble gradient function
			void set_gradientfun(StGradFun gradientfun);

			// set initial guess for x
			void set_initial(const arma::Col<fltp> &x0);

			// set display flag
			void set_use_parallel(const bool use_parallel);
			void set_use_central_diff(const bool central_diff);
			void set_use_linesearch(const bool use_linesearch);
			void set_delta(const fltp delta);
			void set_gamma(const fltp gamma);
			void set_tolx(const fltp tolx);
			void set_num_iter_max(const arma::uword num_iter_max);

			// run optimisation
			void optimise(ShLogPr lg = NullLog::create());

			// get result vector
			arma::Col<fltp> get_result() const;

			// gradient approximation
			static arma::Col<fltp> approximate_gradient(const arma::Col<fltp> &x, const fltp dx, const bool use_parallel, const bool central_diff, StSysFun sysfn);

			// display function
			// static void default_displayfun(const arma::uword n, const fltp fval, const fltp stepnorm);
	};

}}

#endif
