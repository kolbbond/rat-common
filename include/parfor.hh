// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef CMN_PARFOR_HH
#define CMN_PARFOR_HH

// general headers
#include <thread> 
#include <atomic>
#include <future>
#include <vector>

// parallel for loop implementation as template class. 
// Code modified from Andy Thomason:
// http://www.andythomason.com/2016/08/21/c-multithreading-an-effective-parallel-for-loop/

// a parfor loop can be made like this
// parfor(10,20,true,[&](arma::uword idx, int cpu) {
//        printf("task %d running on cpu %d\n", idx, cpu);
//     }
// );

// get number of available cpus for
// parfor loops
// int parfor_num_cpus(){
// 	return std::thread::hardware_concurrency();
// }

// code specific to Rat
namespace rat{namespace cmn{
	// parallel-for template class
	template <class F>
	void parfor(arma::uword begin, arma::uword end, bool enable_parallel, F fn) {
		// make index
		std::atomic<arma::uword> idx;
		idx = begin;

		// fire asynchronous threads
		// don't run as threads when loop only consists of one walk
		if(enable_parallel==true && end-begin>0){
			// get number of cpu's
			int num_cpus = std::thread::hardware_concurrency();
			char* str = std::getenv("RAT_NUM_THREADS");
			if(str!=NULL)num_cpus = std::min(num_cpus, std::stoi(std::string(str)));

			// calculate actual number of threads
			const int num_futures = int(std::min(arma::uword(num_cpus), end-begin+1));

			// get number of available CPU cores
			// int num_cpus = std::thread::hardware_concurrency();
			std::vector<std::future<void> > futures(num_futures);

			// walk over available cpus
			for (int cpu = 0; cpu != num_futures; cpu++) {
				futures[cpu] = std::async(std::launch::async,
					[cpu, &idx, end, &fn]() {
						// keep firing tasks untill run out of idx
						for (;;) {
							// atomic increment of index so that no two threads can do same task
							arma::uword i = idx++; 

							// check if last index
							if (i >= end) break;

							// run loop content with i
							fn(i, cpu);
						}
					}
				);
			}

			// make sure all threads are finished
			for (int cpu = 0; cpu != num_futures; cpu++) {
				futures[cpu].get();
			}
		}

		// just run as regular loop
		else{
			for(arma::uword i = begin;i<end;i++){
				fn(i,0);
			}
		}
	}

}}

#endif