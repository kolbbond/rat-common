// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef CMN_EXTRA_HH
#define CMN_EXTRA_HH

// general headers
#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream

#include <armadillo> 
#include <complex>
#include <cmath>
#include <cassert>
#include <list>

#include <sys/stat.h> // for mkdir
#include <boost/filesystem.hpp>

// specific headers
#include "error.hh"
#include "typedefs.hh"
#include "parfor.hh"

// code specific to Rat
namespace rat{namespace cmn{

	// library of math functions, which are not specifically class related,
	// that are used throughout the code
	class Extra{
		public:
			// displaying of matrices
			static void display_mat(const arma::Mat<std::complex<fltp> > &M);
			static void display_spmat(const arma::SpMat<std::complex<fltp> > &M);
			static void display_mat(const arma::Mat<fltp> &M);
			static void display_spmat(const arma::SpMat<fltp> &M);	
			
			// vector operations for (3xN) matrices
			static arma::Mat<fltp> cross(const arma::Mat<fltp> &R1, const arma::Mat<fltp> &R2);
			static arma::Row<fltp> cross2D(const arma::Mat<fltp> &R1, const arma::Mat<fltp> &R2);
			static arma::Row<fltp> dot(const arma::Mat<fltp> &R1, const arma::Mat<fltp> &R2);
			static arma::Row<fltp> vec_norm(const arma::Mat<fltp> &R1);
			static arma::Mat<fltp> normalize(const arma::Mat<fltp> &V);
			static arma::Mat<fltp> normalize(const arma::Mat<fltp> &V, const bool check_zeros);
			static arma::Row<fltp> vec_angle(const arma::Mat<fltp> &R1, const arma::Mat<fltp> &R2);

			// rotation matrix setup
			static arma::Mat<fltp>::fixed<3,3> create_rotation_matrix(const fltp phi, const fltp theta, const fltp psi);
			static arma::Mat<fltp>::fixed<3,3> create_rotation_matrix(const arma::Col<fltp>::fixed<3> &V, const fltp alpha);
			static arma::Mat<fltp>::fixed<3,3> create_rotation_matrix(const arma::Col<fltp>::fixed<3> &V1,const arma::Col<fltp>::fixed<3> &V2);
			static arma::Mat<fltp>::fixed<3,3> create_rotation_matrix(const fltp ux, const fltp uy, const fltp uz, const fltp alpha);
			static bool is_pure_rotation_matrix(const arma::Mat<fltp>::fixed<3,3> &M, const rat::fltp tol = RAT_CONST(1e-9));
			static arma::Col<fltp>::fixed<4> rotmatrix2angle(const arma::Mat<fltp>::fixed<3,3> &M, const rat::fltp tol = RAT_CONST(1e-15));
			static arma::Mat<fltp>::fixed<3,3> gram_schmidt_orthogonalization(arma::Mat<fltp>::fixed<3,3>&V);
			static arma::Mat<fltp>::fixed<3,3> modified_gram_schmidt_orthogonalization(arma::Mat<fltp>::fixed<3,3>&V);

			// unit vector in specified direction 'X', 'Y' or 'Z'
			static arma::Col<fltp>::fixed<3> null_vec();
			static arma::Col<fltp>::fixed<3> unit_vec(const char direction);
			static char idx2xyz(const arma::uword idx, const bool capitalize = false);
			static arma::uword xyz2idx(const char xyz);
			static bool is_xyz(const char xyz);

			// functions for calculating random coordinates in (3xN) format
			static arma::Mat<fltp> random_coordinates(const fltp xc, const fltp yc, const fltp zc, const fltp size, const arma::uword N);
			static arma::Mat<fltp> random_coordinates(arma::Col<fltp>::fixed<3> Rc, const fltp size, const arma::uword N);

			// some basic math functions
			static arma::Mat<arma::uword> bitshift(const arma::Mat<arma::uword> &M, const int nshift);
			static arma::Mat<arma::uword> modulus(const arma::Mat<arma::uword> &A, const arma::uword i);
			static arma::Mat<fltp> modulus(const arma::Mat<fltp> &A, const fltp v);
			static fltp round(const fltp d);
			static fltp sign(const fltp s);
			static fltp square(const fltp x);

			// least common multiple
			static arma::uword lcm(const arma::Row<arma::uword>& vals, const arma::uword max_iter = 1000);

			// greatest common divisor
			static arma::sword gcd(const arma::sword a, const arma::sword b);

			// function for subdividing arrays
			static arma::Mat<arma::uword> find_sections(const arma::Row<arma::uword> &M);
			static arma::Row<arma::uword> set_sections(const arma::Mat<arma::uword> &indices);
			static arma::Row<arma::uword> expand_indices(const arma::Mat<arma::uword> &indices, const arma::uword num_dim);

			// sort field, applies sorting index to an armadillo field
			template<typename T> static arma::field<T> sort_field(const arma::field<T> &fld, const arma::Col<arma::uword> &sort_index);

			// cell2mat implementation
			template<typename T> static T field2mat(const arma::field<T> &fld);
			template<typename T> static T list2mat(const std::list<T> &fld, const arma::uword dim);

			// field array reversal
			static void reverse_field(arma::field<arma::Mat<fltp> > &A);

			// interpolation function for scalar input XI
			static fltp interp1(const arma::Mat<fltp> &X, const arma::Mat<fltp> &Y, const fltp XI, const char *type = "linear", const bool extrap = false);

			// interpolion function with extrapolation
			static void interp1(const arma::Col<fltp> &x, const arma::Col<fltp> &y, const fltp &xi, fltp &yi, const char *type = "linear", const bool extrap = false);
			static void interp1(const arma::Col<fltp> &x, const arma::Col<fltp> &y, const arma::Col<fltp> &xi, arma::Col<fltp> &yi, const char *type = "linear", const bool extrap = false);

			// fast interpolation function
			static void lininterp1f(const arma::Col<fltp> &x, const arma::Col<fltp> &y, const arma::Col<fltp> &xi, arma::Col<fltp> &yi, const bool extrap = true);

            // interp2 with single value return
            static fltp interp2(const arma::Col<fltp>& x,
                const arma::Col<fltp>& y,
                const arma::Mat<fltp>& v,
                const fltp xi,
                const fltp yi,
                const bool extrap = false);

            // void interp2 with single value reference 
            static void interp2(const arma::Col<fltp>& x,
                const arma::Col<fltp>& y,
                const arma::Mat<fltp>& v,
                const fltp& xi,
                const fltp& yi,
                fltp& vo,
                const bool extrap = false);

            // interp2 with array input
            static void interp2(const arma::Col<fltp>& x,
                const arma::Col<fltp>& y,
                const arma::Mat<fltp>& v,
                const arma::Col<fltp>& xi,
                const arma::Col<fltp>& yi,
                arma::Col<fltp>& vo,
                const bool extrap = false);

            // fast 2D interpolation function
            static void lininterp2f(const arma::Col<fltp>& x,
                const arma::Col<fltp>& y,
                const arma::Mat<fltp>& v,
                const arma::Col<fltp>& xi,
                const arma::Col<fltp>& yi,
                arma::Col<fltp>& vi,
                const bool extrap = false);

            // interp3 with single value return
            static fltp interp3(const arma::Col<fltp>& x,
                const arma::Col<fltp>& y,
                const arma::Col<fltp>& z,
                const arma::Cube<fltp>& v,
                fltp xi,
                fltp yi,
                fltp zi,
                const bool extrap = false);

            // interp3 overload with single value input
            static void interp3(const arma::Col<fltp>& x,
                const arma::Col<fltp>& y,
                const arma::Col<fltp>& z,
                const arma::Cube<fltp>& v,
                const fltp& xi,
                const fltp& yi,
                const fltp& zi,
                fltp& vi,
                const bool extrap);

            // interp3 with array input
            static void interp3(const arma::Col<fltp>& x,
                const arma::Col<fltp>& y,
                const arma::Col<fltp>& z,
                const arma::Cube<fltp>& v,
                const arma::Col<fltp>& xi,
                const arma::Col<fltp>& yi,
                const arma::Col<fltp>& zi,
                arma::Col<fltp>& vo,
                const bool extrap = false);

            // fast 3D interpolation function
            static void lininterp3f(const arma::Col<rat::fltp>& x,
                const arma::Col<rat::fltp>& y,
                const arma::Col<rat::fltp>& z,
                const arma::Cube<rat::fltp>& v,
                const arma::Col<rat::fltp>& xi,
                const arma::Col<rat::fltp>& yi,
                const arma::Col<rat::fltp>& zi,
                arma::Col<rat::fltp>& vi,
                const bool extrap = false);

			// angle unwrapping
			static arma::Row<fltp> unwrap_angles(const arma::Row<fltp>& alpha, const rat::fltp angle_tol = 5*arma::Datum<rat::fltp>::tau/360);

			// smoothing
			static arma::Row<fltp> smooth_core(const arma::Row<fltp>&ell, const arma::Row<fltp>&alpha, const fltp window_length, const bool use_parallel);
			static arma::Row<fltp> smooth(const arma::Row<fltp>&ell, const arma::Row<fltp>&alpha, const fltp window_length, const arma::uword num_passes = 1, const bool use_parallel = false);

			// unique rows
			static arma::Mat<arma::uword> unique_rows(const arma::Mat<arma::uword> &M);

			// parallel sorting
			static arma::Row<arma::uword> parsort(const arma::Row<arma::uword> &vals);

			// armadillo unique functions but with tolerance
			static arma::Col<arma::uword> find_unique(const arma::Mat<fltp>& vals, const bool ascending_indices = true, const fltp tol = RAT_CONST(0.0));

			// making directory
			static void create_directory(const boost::filesystem::path &dirname);

			// cumulative trapzoidal integration
			static arma::Mat<fltp> cumtrapz(const arma::Row<fltp> &t, const arma::Mat<fltp> &v, const arma::uword dim = 1);

			// poisson distribution
			static arma::Row<arma::uword> poisson_random(const arma::uword num_values, const rat::fltp lambda);

			// mesh operations
			static arma::Row<arma::uword> combine_nodes(arma::Mat<fltp> &Rn, const fltp delta = 1024*arma::Datum<fltp>::eps);
			static arma::Row<arma::uword> combine_nodes(arma::Mat<fltp> &Rn, arma::Mat<fltp> &data, const fltp delta = 1024*arma::Datum<fltp>::eps);

			// graph connectivity search (expects NX2 matrix of indices)
			static arma::Row<arma::uword> conncomp(const arma::Mat<arma::uword> &n12, const arma::uword num_nodes);

			// calculate curvature vectors from coordinates
			static arma::Mat<fltp> calc_curvature(const arma::Mat<fltp> &R);

			// version tag
			static std::string create_version_tag(const int major_version, const int minor_version, const int patch_version);

			// search functions
			static arma::Col<arma::uword> ismember(const arma::Row<arma::uword>&A, const arma::Row<arma::uword>&B);
	};


	// sort field array
	template<typename T> 
	arma::field<T> Extra::sort_field(
		const arma::field<T> &fld, 
		const arma::Col<arma::uword> &sort_index){
		
		// check input
		assert(arma::max(sort_index)<fld.n_elem);
		assert(fld.n_rows==1);

		// field sorting
		arma::field<T> new_fld(1, sort_index.n_elem);

		// perform sorting
		for(arma::uword i=0;i<sort_index.n_elem;i++)
			new_fld(i) = fld(sort_index(i));

		// return the new field array
		return new_fld;
	}

	// matlab cell2mat implementation for armadillo
	template<typename T> 
	T Extra::field2mat(
		const arma::field<T> &fld){
		
		// check if it is necessary to make a new matrix
		if (fld.n_cols==1 && fld.n_rows==1){
			return fld(0,0);
		}

		// get sizes of all submatrices
		arma::Mat<arma::uword> num_rows(fld.n_rows,fld.n_cols);
		arma::Mat<arma::uword> num_cols(fld.n_rows,fld.n_cols);
		for(arma::uword i=0;i<fld.n_rows;i++){
			for(arma::uword j=0;j<fld.n_cols;j++){
				num_rows(i,j) = fld(i,j).n_rows;
				num_cols(i,j) = fld(i,j).n_cols;
			}
		}
		
		// check if submatrices fit
		arma::Col<arma::uword> num_rows_first_col = num_rows.col(0);
		arma::Row<arma::uword> num_cols_first_row = num_cols.row(0);

		// check if matrix fits
		assert(arma::all(arma::all(num_rows_first_col-num_rows.each_col()==0)));
		assert(arma::all(arma::all(num_cols_first_row-num_cols.each_row()==0)));

		// allocate output matrix
		T mt(arma::sum(num_rows_first_col),arma::sum(num_cols_first_row));

		// fill output matrix
		// walk over rows
		arma::uword row_start = 0;
		for(arma::uword i=0;i<fld.n_rows;i++){
			// walk over columns
			arma::uword col_start = 0;
			for(arma::uword j=0;j<fld.n_cols;j++){
				// check if this cell is not empty
				if(num_rows(i,j)>0 && num_cols(i,j)>0){
					// calculate indexes
					arma::uword row_end = row_start + num_rows_first_col(i) - 1;
					arma::uword col_end = col_start + num_cols_first_row(j) - 1;
				
					// insert into submatrix
					mt.submat( row_start, col_start, row_end, col_end) = fld(i,j);

					// increment column index
					col_start += num_cols_first_row(j);
				}		
			}

			// increment row index
			row_start += num_rows_first_col(i);
		}

		// return output matrix
		return mt;
	}

	// convert a list of matrices with identical 
	// number of rows into a single matrix
	template<typename T> 
	T Extra::list2mat(const std::list<T> &ls, const arma::uword dim){
		// empty case
		if(ls.empty())return{};

		// set index
		arma::uword idx = 0;

		// count number of elements
		arma::Col<arma::uword> num_rows(ls.size());
		arma::Col<arma::uword> num_cols(ls.size());
		for(auto it=ls.begin();it!=ls.end();it++){
			const T& M = (*it);
			num_rows(idx) = M.n_rows;
			num_cols(idx) = M.n_cols;
			idx++;
		}

		if(dim==0){
			// check
			if(arma::any(num_cols!=num_cols(0)))rat_throw_line("not all matrices have same number of cols");

			// allocate output
			T M(arma::accu(num_rows),num_cols(0));

			// fill matrix
			idx = 0;
			for(auto it=ls.begin();it!=ls.end();it++){
				const arma::uword nc = (*it).n_rows;
				M.rows(idx,idx+nc-1) = (*it); idx+=nc;
			}

			// return matrix
			return M;
		}

		else if(dim==1){
			// check
			if(arma::any(num_rows!=num_rows(0)))rat_throw_line("not all matrices have same number of rows");

			// allocate output
			T M(num_rows(0),arma::accu(num_cols));

			// fill matrix
			idx = 0;
			for(auto it=ls.begin();it!=ls.end();it++){
				const arma::uword nc = (*it).n_cols;
				M.cols(idx,idx+nc-1) = (*it); idx+=nc;
			}

			// return matrix
			return M;
		}

		else{
			rat_throw_line("dim should either be zero or one for col and row respectively");
		}
	}

}}

#endif
