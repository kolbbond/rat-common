// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// this file was evolved from:
// http://math.nist.gov/iml++/

// include guard
#ifndef CMN_GMRES_HH
#define CMN_GMRES_HH

// general headers
#include <cmath> 
#include <functional>
#include <armadillo>
#include <cassert>
#include <iostream>
#include <memory>

// specific headers
#include "typedefs.hh"
#include "log.hh"

// code specific to Rat
namespace rat{namespace cmn{
	// shared pointer definition
	typedef std::shared_ptr<class GMRES> ShGMRESPr;

	// define system function
	typedef std::function<arma::Col<fltp>(const arma::Col<fltp>) > GmresSysFun;
	typedef std::function<arma::Col<fltp>(const arma::Col<fltp>) > GmresPrecFun;

	// harmonics class template
	class GMRES{
		// properties
		private:
			// settings
			arma::uword num_restart_ = 32;
			arma::uword num_iter_max_ = 150;
			fltp tol_ = RAT_CONST(1e-6);

			// functions
			GmresSysFun systemfun_ = NULL;
			GmresPrecFun precfun_ = NULL;
			
			// upper Hessenberg
			arma::Mat<fltp> H_;

			// other internal storage
			fltp normb_;
			arma::Col<fltp> s_;
			arma::Col<fltp> cs_;
			arma::Col<fltp> sn_;
			arma::field<arma::Col<fltp> > v_;

			// additional output
			int flag_;
			fltp relres_ = arma::datum::inf;
			arma::uword num_iter_ = 100000;

		// methods
		public:
			// constructor
			GMRES();

			// factory
			static ShGMRESPr create();

			// setting of functions
			void set_systemfun(GmresSysFun fn);
			void set_precfun(GmresPrecFun fn);
			
			// settings
			void set_tol(const fltp tol);
			void set_num_iter_max(const arma::uword num_iter_max);
			void set_num_restart(const arma::uword num_restart);

			// solve system (x is output but also contains initial guess)
			void solve(arma::Col<fltp> &x, const arma::Col<fltp> &b, ShLogPr lg = NullLog::create());
			void update(arma::Col<fltp> &x, const int k);

			// helper functions
			static void apply_plane_rot(fltp &dx, fltp &dy, fltp &cs, fltp &sn);
			static void gen_plane_rot(fltp &dx, fltp &dy, fltp &cs, fltp &sn);

            // getters
            fltp get_residual();
            arma::uword get_num_iter();
	};

}}

#endif
