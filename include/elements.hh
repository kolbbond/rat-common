// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef CMN_ELEMENTS_HH
#define CMN_ELEMENTS_HH

// general headers
#include <armadillo> 
#include <complex>
#include <cmath>
#include <cassert>

// specific headers
#include "typedefs.hh"

// code specific to Rat
namespace rat{namespace cmn{
	// QUADRILATERALS
	// 4 node quadrilaterals 
	
	// definition:
	// nodes:
	// o-->xi 
	// | 0---1
	// nu|   |
	//   3---2

	// that are used throughout the code
	class Quadrilateral{
		// methods
		public:
			// definitions
			typedef arma::Col<arma::uword>::fixed<4> ElementVector;
			typedef arma::Mat<arma::uword>::fixed<2,3> TriangleConversionMatrix;
			typedef arma::Mat<arma::sword>::fixed<4,2> CornerNodeMatrix;
			typedef arma::Mat<arma::uword>::fixed<4,2> EdgeMatrix;

			// conversion to triangles
			static TriangleConversionMatrix triangle_conversion_matrix();

			// definition matrices of quad
			static CornerNodeMatrix get_corner_nodes();
			static EdgeMatrix get_edges();

			// check if nodes are in plane
			static bool check_nodes(const arma::Mat<fltp> &Rn, const fltp tol = RAT_CONST(1e-5));

			// shape function
			static arma::Mat<fltp> shape_function(const arma::Mat<fltp> &Rq);
			static arma::Mat<fltp> shape_function_derivative(const arma::Mat<fltp> &Rq);

			// create gauss points in quadrilateral coordinates
			static arma::Mat<fltp> create_gauss_points(const arma::sword num_gauss);

			// coordinate transformations
			static arma::Mat<fltp> quad2cart(const arma::Mat<fltp> &Rn, const arma::Mat<fltp> &Rq);
			static arma::Mat<fltp> cart2quad(const arma::Mat<fltp> &Rn, const arma::Mat<fltp> &Rc, const fltp tol);
			static arma::Mat<fltp> quad2cart_derivative(const arma::Mat<fltp> &Rn, const arma::Mat<fltp> &Rq, const arma::Mat<fltp> &phi);
			
			// jacobian functions
			static arma::Mat<fltp> shape_function_jacobian(const arma::Mat<fltp> &Rn,const arma::Mat<fltp> &dN);
			static arma::Row<fltp> jacobian2determinant(const arma::Mat<fltp>& J);
			static arma::Mat<fltp> invert_jacobian(const arma::Mat<fltp>&J, const arma::Row<fltp>&Jdet);

			// computing grid setup
			static void setup_source_grid(arma::Mat<fltp> &Rq, arma::Row<fltp> &w, const arma::Col<fltp>::fixed<2> &Rqs, const arma::Row<fltp> &xg, const arma::Row<fltp> &wg);

			// function for calculating face normals of a mesh
			static arma::Mat<fltp> calc_face_normals(const arma::Mat<fltp> &Rn, const arma::Mat<arma::uword> &n);
			static arma::Mat<fltp> calc_node_normals(const arma::Mat<fltp> &Rn, const arma::Mat<arma::uword> &n);

			// calculate face centroids
			static arma::Mat<fltp> calc_face_centroids(const arma::Mat<fltp> &Rn, const arma::Mat<arma::uword> &n);

			// function for converting a quadrilateral mesh to triangular mesh
			static arma::Mat<arma::uword> quad2tri(const arma::Mat<arma::uword> &n);
			static arma::Mat<arma::uword> quad2tri(const arma::Mat<arma::uword> &n, const arma::Mat<fltp> &Rn);

			// extract smooth surfaces for rendering
			static arma::Row<arma::uword> separate_smooth_surfaces(arma::Mat<fltp> &Rn, arma::Mat<arma::uword> &n, const fltp angle_treshold = RAT_CONST(20.0)*2*arma::Datum<fltp>::pi/RAT_CONST(360.0));
			
			// find edges
			static arma::Mat<arma::uword> find_edges(const arma::Mat<fltp> &Rn, const arma::Mat<arma::uword> &n, const fltp angle_treshold = RAT_CONST(20.0)*2*arma::Datum<fltp>::pi/RAT_CONST(360.0));

			// invert elements
			static arma::Mat<arma::uword> invert_elements(const arma::Mat<arma::uword>&n);

			// calculate surface area
			static fltp calc_area(const arma::Mat<fltp> &Rn);
			static arma::Row<fltp> calc_area(const arma::Mat<fltp> &Rn, const arma::Mat<arma::uword> &n);
	};


	// HEXAHEDRONS
	// six node hexahedron
	
	// definition:
	// nodes:
	// o-->xi 
	// | 0---1     4---5
	// nu|   |     |   |
	//   3---2     7---6
	//   mu=-1     mu=1:

	// sides:
	// Side Node-1 Node-2 Node-3 Node-4
	// Side-1 (S1) 0 1 5 4 in xi, mu
	// Side-2 (S2) 1 2 6 5 in nu, mu
	// Side-3 (S3) 2 3 7 6 in xi, mu
	// Side-4 (S4) 3 0 4 7 in nu, mu
	// Side-5 (S5) 0 3 2 1 in xi, nu
	// Side-6 (S6) 4 5 6 7 in xi, nu
	class Hexahedron{
		// methods
		public:
			// data types
			typedef arma::Col<arma::uword>::fixed<8> ElementVector;
			typedef arma::Col<fltp>::fixed<6> FaceDataVector;
			typedef arma::Mat<fltp>::fixed<6,6> Face2FaceDataMatrix;

			// definition of matrices
			typedef arma::Mat<arma::sword>::fixed<8,3> CornerNodeMatrix;
			typedef arma::Mat<arma::uword>::fixed<6,4> FaceMatrix;
			typedef arma::Mat<arma::uword>::fixed<12,2> EdgeMatrix;
			typedef arma::Mat<arma::sword>::fixed<6,3> FaceNodeMatrix;
			typedef arma::Mat<arma::sword>::fixed<6,3> FaceNormalMatrix;
			typedef arma::Col<arma::uword>::fixed<6> FaceNormalDirVector;
			typedef arma::Mat<arma::uword>::fixed<6,3> FaceDimMatrix;
			typedef arma::Col<arma::sword>::fixed<6> FaceDirVector;
			typedef arma::Mat<arma::uword>::fixed<4,6> FaceEdgeMatrix;
			typedef arma::Mat<arma::uword>::fixed<4,6> FaceEdgeReverseMatrix;
			typedef arma::Mat<arma::uword>::fixed<5,4> TetrahedronConversionMatrix;
			typedef arma::Mat<arma::uword>::fixed<12,4> SpecialTetrahedronConversionMatrix;

			// definition matrices of hexahedron
			static CornerNodeMatrix get_corner_nodes();
			static FaceMatrix get_faces();
			static EdgeMatrix get_edges();
			static FaceNodeMatrix get_face_nodes();
			static FaceNormalMatrix get_facenormal();
			static FaceNormalDirVector get_facenormal_dir();
			static FaceDimMatrix get_facedim();
			static FaceDirVector get_facedir();
			static FaceEdgeMatrix get_face_edges();
			static FaceEdgeReverseMatrix get_face_edges_reverse();

			// hexahedron conversion matrices
			static TetrahedronConversionMatrix tetrahedron_conversion_matrix();
			static SpecialTetrahedronConversionMatrix tetrahedron_conversion_matrix_special();
			
			// refine hexahedral mesh to tetrahedral mesh
			static void refine_mesh_to_tetrahedron(arma::Mat<fltp>&Rn, arma::Mat<arma::uword>&n);

			// function for creating gauss points in quadrilateral coordinates
			static arma::Mat<fltp> create_gauss_points(const arma::sword num_gauss);

			// shape function and its derivatives
			static arma::Mat<fltp> shape_function(const arma::Mat<fltp> &Rq);
			static arma::Mat<fltp> nedelec_hdiv_shape_function(const arma::Mat<fltp> &Rq);
			// static arma::Mat<fltp> nedelec_hcurl_shape_function(const arma::Mat<fltp> &Rq);
			static arma::Mat<fltp> shape_function_derivative(const arma::Mat<fltp> &Rq);
			static arma::Mat<fltp> shape_function_derivative_cart(const arma::Mat<fltp> &Rn, const arma::Mat<fltp> &Rq);

			// jacobian functions
			static arma::Mat<fltp> jacobian_t(const arma::Mat<fltp> &J);
			static arma::Mat<fltp> shape_function_jacobian(const arma::Mat<fltp> &Rn,const arma::Mat<fltp> &dN);
			static arma::Row<fltp> jacobian2determinant(const arma::Mat<fltp> &J);
			static arma::Mat<fltp> invert_jacobian(const arma::Mat<fltp> &J, const arma::Row<fltp> &Jdet);

			// extrapolation to nodes
			static arma::Mat<fltp> gauss2nodes(const arma::Mat<fltp> &Rn,const arma::Mat<fltp> &Rqg,const arma::Row<fltp> &wg, const arma::Mat<fltp> &Vg);
			static arma::Mat<fltp> gauss2nodes(const arma::Mat<fltp> &Rn,const arma::Mat<arma::uword> &n,const arma::Mat<fltp> &Vg,	const arma::Mat<fltp> &Rqg, const arma::Row<fltp> &wg, const arma::uword num_nodes);

			// quad to cart functions for vector spaces
			static arma::Mat<fltp> quad2cart_contravariant_piola(const arma::Mat<fltp> &Vq, const arma::Mat<fltp> &J, const arma::Row<fltp> &Jdet);
			static arma::Mat<fltp> quad2cart_covariant_piola(const arma::Mat<fltp> &Vq, const arma::Mat<fltp> &Jinv);

			// quadrilateral coordinates
			static arma::Mat<fltp> quad2cart(const arma::Mat<fltp> &Rn, const arma::Mat<fltp> &Rq);
			static arma::Mat<fltp> cart2quad(const arma::Mat<fltp> &Rn, const arma::Mat<fltp> &Rc, const fltp tol);

			// derivatives of quadrilateral coordinates
			static arma::Mat<fltp> derivative2cart(const arma::Mat<fltp>&Jinv,const arma::Row<fltp>&Jdet,const arma::Mat<fltp>&dN);
			static arma::Mat<fltp> quad2cart_derivative(const arma::Mat<fltp> &Rn, const arma::Mat<fltp> &Rq, const arma::Mat<fltp> &phi);
			static arma::Mat<fltp> quad2cart_curl(const arma::Mat<fltp> &Rn, const arma::Mat<fltp> &Rq, const arma::Mat<fltp> &V);

			// functions for checking inside or outside
			static arma::Row<arma::uword> is_inside(const arma::Mat<fltp> &R, const arma::Mat<fltp> &Rn, const arma::Mat<arma::uword> &n);

			// skewedness calculation (form of mesh quality)
			static arma::Row<fltp> calc_skewedness(const arma::Mat<fltp>&Rn, const arma::Mat<arma::uword>&n);
			static arma::Row<fltp> calc_aspect_ratio(const arma::Mat<fltp>&Rn, const arma::Mat<arma::uword>&n);

			// volume calculation
			static fltp calc_volume(const arma::Mat<fltp> &Rn);
			static arma::Row<fltp> calc_volume(const arma::Mat<fltp> &Rn, const arma::Mat<arma::uword> &n);

			// extract surface quadrilaterals from volume mesh
			static arma::Mat<arma::uword> extract_surface(const arma::Mat<arma::uword> &n);

			// check if faces are correct
			static arma::Row<arma::uword> is_clockwise(const arma::Mat<fltp> &R, const arma::Mat<arma::uword> &n);
			static arma::Mat<arma::uword> invert_elements(const arma::Mat<arma::uword>&n);

			// grid for avoiding singularities
			static void setup_source_grid(arma::Mat<fltp> &Rqgrd, arma::Row<fltp> &wgrd, const arma::Col<fltp>::fixed<3> &Rqs, const arma::Row<fltp> &xg, const arma::Row<fltp> &wg);

			// some default shapes
			static void create_sphere(arma::Mat<fltp>&Rn, arma::Mat<arma::uword>&n, arma::Mat<arma::uword>&s, const fltp sphere_radius, const fltp element_size, const fltp core_size = RAT_CONST(0.7));
	};


	// TETRAHEDRONS
	// four node tetrahedron

	// definition:
	// nodes:
	// o-->xi 
	// | 0---1     3
	// nu| /
	//   2
	//   mu=-1     mu=1:
	
	// tetrahedron
	class Tetrahedron{
		// methods
		public:
			// datatypes
			typedef arma::Col<arma::uword>::fixed<4> ElementVector;
			typedef arma::Col<fltp>::fixed<4> FaceDataVector;
			typedef arma::Mat<fltp>::fixed<4,4> Face2FaceDataMatrix;

			// definitions
			typedef arma::Mat<arma::uword>::fixed<4,3> FaceMatrix;
			typedef arma::Mat<arma::sword>::fixed<4,3> CornerNodeMatrix;
			typedef arma::Mat<arma::uword>::fixed<6,2> EdgeMatrix;

			// definition matrices
			static FaceMatrix get_faces();
			static EdgeMatrix get_edges();
			static CornerNodeMatrix get_corner_nodes();

			// additional functions
			static arma::Row<fltp> calc_volume(const arma::Mat<fltp> &Rn, const arma::Mat<arma::uword> &n);
			static arma::Row<fltp> special_determinant(const arma::Mat<fltp> &R1, const arma::Mat<fltp> &R2, const arma::Mat<fltp> &R3, const arma::Mat<fltp> &R4);
			static arma::Row<arma::uword> is_inside(const arma::Mat<fltp> &R, const arma::Mat<fltp> &Rn, const arma::Mat<arma::uword> &n, const fltp tolerance = 1e-8);

			// function for creating gauss points in quadrilateral coordinates
			static arma::Mat<fltp> create_gauss_points(const arma::sword num_gauss);

			// shape function and its derivatives
			static arma::Mat<fltp> shape_function(const arma::Mat<fltp> &Rq);
			static arma::Mat<fltp> shape_function_derivative(const arma::Mat<fltp> &Rq);
			static arma::Mat<fltp> shape_function_derivative_cart(const arma::Mat<fltp> &Rn, const arma::Mat<fltp> &Rq);
			static arma::Mat<fltp> shape_function_jacobian(const arma::Mat<fltp> &Rn,const arma::Mat<fltp> &dN);
			static arma::Row<fltp> jacobian2determinant(const arma::Mat<fltp> &J);
			static arma::Mat<fltp> invert_jacobian(const arma::Mat<fltp> &J, const arma::Row<fltp> &Jdet);
			static arma::Mat<fltp> nedelec_hdiv_shape_function(const arma::Mat<fltp> &Rq);

			// skewedness calculation (form of mesh quality)
			static arma::Row<fltp> calc_skewedness(const arma::Mat<fltp>&Rn, const arma::Mat<arma::uword>&n);
			static arma::Row<fltp> calc_aspect_ratio(const arma::Mat<fltp>&Rn, const arma::Mat<arma::uword>&n);

			// quad to cart functions for vector spaces
			static arma::Mat<fltp> quad2cart_contravariant_piola(const arma::Mat<fltp> &Vq, const arma::Mat<fltp> &J, const arma::Row<fltp> &Jdet);
			static arma::Mat<fltp> quad2cart_covariant_piola(const arma::Mat<fltp> &Vq, const arma::Mat<fltp> &Jinv);

			// quadrilateral coordinates
			static arma::Mat<fltp> derivative2cart(const arma::Mat<fltp>&Jinv,const arma::Row<fltp>&Jdet,const arma::Mat<fltp>&dN);
			static arma::Mat<fltp> quad2cart_derivative(const arma::Mat<fltp> &Rn, const arma::Mat<fltp> &Rq, const arma::Mat<fltp> &phi);
			static arma::Mat<fltp> quad2cart_curl(const arma::Mat<fltp> &Rn, const arma::Mat<fltp> &Rq, const arma::Mat<fltp> &V);
			static arma::Mat<fltp> quad2cart(const arma::Mat<fltp> &Rn, const arma::Mat<fltp> &Rq);
			static arma::Mat<fltp> cart2quad(const arma::Mat<fltp> &Rn, const arma::Mat<fltp> &Rc, const fltp tol);
			
			// check if faces are correct
			static arma::Row<arma::uword> is_clockwise(const arma::Mat<fltp> &R, const arma::Mat<arma::uword> &n);
			static arma::Mat<arma::uword> invert_elements(const arma::Mat<arma::uword>&n);

			// extract surface triangles from volume mesh
			static arma::Mat<arma::uword> extract_surface(const arma::Mat<arma::uword> &n);
			static arma::Mat<arma::uword> extract_surface(const arma::Mat<fltp>&Rn, const arma::Mat<arma::uword> &n);

			// in place mesh refinement
			static void refine_mesh(arma::Mat<fltp>&Rn, arma::Mat<arma::uword>&n, const arma::uword num_refine = 1llu);
			
			// some default shapes
			static void create_icosahedron(arma::Mat<fltp>&Rn, arma::Mat<arma::uword>&n);
			static void create_sphere(arma::Mat<fltp>&Rn, arma::Mat<arma::uword>&n, arma::Mat<arma::uword>&s, const fltp sphere_radius, const fltp element_size);
	};


	// LINE ELEMENT
	// 2 node line elements

	// definition:
	// nodes:
	// o-->xi 
	// 0---1
	
	// line
	class Line{
		// methods
		public:
			// definitions
			typedef arma::Col<arma::uword>::fixed<2> ElementVector;
			typedef arma::Mat<arma::sword>::fixed<2,1> CornerNodeMatrix;

			// definition matrices of quad
			static CornerNodeMatrix get_corner_nodes();

			// create gauss points in quadrilateral coordinates
			static arma::Mat<fltp> create_gauss_points(const arma::sword num_gauss);

			// shape function
			static arma::Mat<fltp> shape_function(const arma::Mat<fltp> &Rq);
			static arma::Mat<fltp> shape_function_derivative(const arma::Mat<fltp> &Rq);
			
			// coordinate transformations
			static arma::Mat<fltp> quad2cart(const arma::Mat<fltp> &Rn, const arma::Mat<fltp> &Rq);
			
			// jacobian functions
			static arma::Mat<fltp> shape_function_jacobian(const arma::Mat<fltp> &Rn,const arma::Mat<fltp> &dN);
			static arma::Row<fltp> jacobian2determinant(const arma::Mat<fltp>& J);
			static arma::Mat<fltp> invert_jacobian(const arma::Mat<fltp>&J, const arma::Row<fltp> &Jdet);

			// calculate length
			static arma::Row<fltp> calc_length(const arma::Mat<fltp> &Rn, const arma::Mat<arma::uword> &n);
	};


	// TRIANGLE ELEMENT
	// 3 node triangle

	// definition:
	// nodes:
	// o-->xi 
	// | 0---1
	// nu| /
	//   2
	//   mu=-1     mu=1:
	
	// triangle
	class Triangle{
		// methods
		public:
			// definitions
			typedef arma::Col<arma::uword>::fixed<3> ElementVector;
			typedef arma::Mat<arma::uword>::fixed<3,2> EdgeMatrix;
			typedef arma::Mat<arma::sword>::fixed<3,2> CornerNodeMatrix;

			// definition of triangle
			static EdgeMatrix get_edges();
			static CornerNodeMatrix get_corner_nodes();

			// function for calculating face normals of a mesh
			static arma::Mat<fltp> calc_face_normals(const arma::Mat<fltp> &Rn, const arma::Mat<arma::uword> &n);
			static arma::Mat<fltp> calc_node_normals(const arma::Mat<fltp> &Rn, const arma::Mat<arma::uword> &n);

			// function for creating gauss points in quadrilateral coordinates
			static arma::Mat<fltp> create_gauss_points(const arma::sword num_gauss);
			static arma::Mat<fltp> create_gauss_points_catmull_clark(const arma::sword num_gauss);

			// shape function and its derivatives
			static arma::Mat<fltp> shape_function(const arma::Mat<fltp> &Rq);
			static arma::Mat<fltp> quad2cart(const arma::Mat<fltp> &Rn, const arma::Mat<fltp> &Rq);
			static arma::Mat<fltp> shape_function_derivative(const arma::Mat<fltp> &Rq);

			// jacobian
			static arma::Mat<fltp> shape_function_jacobian(const arma::Mat<fltp>&Rn, const arma::Mat<fltp>&dN);
			static arma::Mat<fltp> invert_jacobian(const arma::Mat<fltp> &J, const arma::Row<fltp> &Jdet);
			static arma::Row<fltp> jacobian2determinant(const arma::Mat<fltp>& J);
			
			// find edges
			static arma::Mat<arma::uword> find_edges(const arma::Mat<fltp> &Rn, const arma::Mat<arma::uword> &n, const fltp angle_treshold = RAT_CONST(20.0)*2*arma::Datum<fltp>::pi/RAT_CONST(360.0));

			// calcultae face area of the triangle
			static arma::Row<fltp> calc_area(const arma::Mat<fltp> &Rn,const arma::Mat<arma::uword> &n);

			// extract smooth surfaces for rendering
			static arma::Row<arma::uword> separate_smooth_surfaces(arma::Mat<fltp> &Rn, arma::Mat<arma::uword> &n, const fltp angle_treshold = RAT_CONST(20.0)*2*arma::Datum<fltp>::pi/RAT_CONST(360.0));

			// invert elements of a mesh
			static arma::Mat<arma::uword> invert_elements(const arma::Mat<arma::uword>&n);
	};

}}

#endif