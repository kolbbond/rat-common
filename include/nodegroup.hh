// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef CMN_NODE_GROUP_HH
#define CMN_NODE_GROUP_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <list>
#include <json/json.h>

#include "node.hh"

// code specific to Rat
namespace rat{namespace cmn{

	// shared pointer definition
	typedef std::shared_ptr<class NodeGroup> ShNodeGroupPr;
	typedef arma::field<ShNodeGroupPr> ShNodeGroupPrList;


	// circle distance function for distmesh
	class NodeGroup: public Node{
		protected:
			// collection of nodes
			std::map<arma::uword,ShNodePr> nodes_;

		public:
			// constructor
			NodeGroup();

			// factory
			static ShNodeGroupPr create();

			// tree structure
			virtual ShNodePr get_tree_node(const arma::uword index) const override;
			virtual arma::uword add_tree_node(const ShNodePr &node) override;
			virtual bool delete_tree_node(const arma::uword index) override;
			virtual arma::uword get_num_tree_nodes() const override;
			virtual void reindex() override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, SList &list) const override;
			virtual void deserialize(const Json::Value &js, std::map<arma::uword,ShNodePr> &list, const NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};


}}

#endif
