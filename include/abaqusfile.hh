// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include guard
#ifndef CMN_ABAQUS_FILE_HH
#define CMN_ABAQUS_FILE_HH

// general headers
#include <boost/filesystem.hpp>
#include <armadillo> 
#include <cassert>
#include <memory>
#include <set>
#include <iomanip>
#include <list>

#include "typedefs.hh"

// code specific to Rat
namespace rat{namespace cmn{
	// shared pointer definition for log
	typedef std::shared_ptr<class AbaqusFile> ShAbaqusFilePr;

	// logging to the terminal
	class AbaqusFile{
		// properties
		private:
			// settings
			std::ofstream fid_;

			// part list
			arma::uword node_offset_ = 0;
			arma::uword elem_offset_ = 0;
			std::list<std::string> parts_;
			std::map<std::string, arma::uword> materials_;

		// methods 
		public:
			// constructor
			explicit AbaqusFile(
				const boost::filesystem::path &fname, 
				const std::string &model_name);

			// destructor
			~AbaqusFile();

			// factory
			static ShAbaqusFilePr create(
				const boost::filesystem::path &fname, 
				const std::string &model_name);

			// write nodes
			void write_part(
				const std::string &part_name, 
				const arma::Mat<fltp> &Rn, 
				const arma::Mat<arma::uword> &n, 
				const std::string &material_name);
	};

}}

#endif